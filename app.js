var express = require('express')
var bodyParser = require('body-parser')
var compression = require('compression')
var cors = require('cors')
var app = express()
var fs = require("fs");
var moment = require('moment')
var path = require('path');
require('dotenv').config();
var sharp = require('sharp')

app.use(bodyParser.json({ limit: "80mb" }));
app.use(bodyParser.urlencoded({
    parameterLimit: 100000,
    limit: '80mb',
    extended: true
}));
app.use(bodyParser());


process.env.PUBLIC_DIR = path.join(__dirname, 'public')
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors())
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Credentials', 'true')
    next()
})

app.use(compression());

app.get('/fms/v1/', (req, res) => {
    return res.status(200).send({ message: "FMS started" })
})

app.post('/fms/v1/upload', (req, res) => {
    var file = req.body.file || ""
    var name = req.body.name || "img-" + moment().unix() + ".jpg"
    var realFile = Buffer.from(file, "base64")
    if (!fs.existsSync(process.env.PUBLIC_DIR)) {
        fs.mkdirSync(process.env.PUBLIC_DIR);
    }

    if (!fs.existsSync(process.env.PUBLIC_DIR + "/uploads")) {
        fs.mkdirSync(process.env.PUBLIC_DIR + "/uploads");
    }

    fs.writeFile(path.join(process.env.PUBLIC_DIR, "/", name), realFile, (err) => {

        if (err) {
            return res.status(400).send({ status: 400, url: "", message: err })
        }
        var newName = moment().unix() + "-" + name
        var newPath = process.env.PUBLIC_DIR + '/uploads/' + newName

        sharp(path.join(process.env.PUBLIC_DIR, "/", name))
            .rotate()
            .resize(580, 400, { quality: 90, fit: sharp.fit.contain, })
            .toFile(newPath)
            .then(() => {
                var path = "uploads/" + newName
                return res.status(200).send({ status: 200, url: path, message: "Uploaded !" })
            });
    })
})


app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    var message = {}
    message.message = err.message
    message.status = err.status
    res.locals.error = message;

    // render the error page
    res.status(err.status || 500).send(message);
});

module.exports = app